import React from 'react'
import { Link } from 'react-router-dom'

let linkButton = null;

const characterCard = (props) => {
    linkButton = (
    <Link to={{ 
        pathname: '/character/' + props.id }} 
        className="btn btn-light">
        View
    </Link>);

    return (
        <div className="col-xs-12 col-sm-6 col-md-4">
            <div className="card CharacterCard mb-3" id="CharacterCard">
                <img src={props.image} alt={props.name} className="card-img-top"/>

                <div className="card-body">
                    <div>
                        <div>
                            <h4 className="card-title">{props.name}</h4>
                        </div>
                        <div>
                        <p><b>Species: </b></p> <p className="card-info">{props.species}</p><br />
                        </div>
                        
                        <div>
                            <p><b>Status: </b></p> <p className="card-info">{props.status}</p><br />
                        </div>
                        <div>
                            <p><b>Gender: </b></p> <p className="card-info">{props.gender}</p><br />
                        </div>
                        <div>
                            <p><b>Location: </b></p> <p className="card-info">{props.location}</p><br />
                        </div>
                        <div>
                            <p><b>Place of origin: </b></p> <p className="card-info">{props.origin}</p><br /> 
                        </div>
                    </div>
                    {props.showLink ? linkButton : null}                   
                </div>
            </div>
        </div>
    );

}

export default characterCard;