import React, { Component } from 'react'


class LocationSearch extends Component {

    constructor(props) {
        super(props)
        this.state = {
            rickMorty: [],
			locationFilter: ''
        }

        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.searchButtonClicked = this.searchButtonClicked.bind(this);
    }


    handleLocationChange(e) {
        const input = e.target.value
        this.setState({ locationFilter: input })
    }

    searchButtonClicked() {
        if (this.props.onClick) {
            this.props.onClick(this.state.locationFilter)
        }
    }

    render() {
        return (
            <>
                <div className="search">
                    <input type="text"
                            onChange={this.handleLocationChange}
                            className="form-control"
                            placeholder="Search for a Rick &
                            Morty location..." />
                        <button className="btn btn-light" onClick={this.searchButtonClicked}>Search</button>
                </div>
            </>
        )
    }
}

export default LocationSearch