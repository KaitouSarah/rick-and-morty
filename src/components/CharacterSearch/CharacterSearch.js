import React, { Component } from 'react'


class CharacterSearch extends Component {

    constructor(props) {
        super(props)
        this.state = {
            rickMorty: [],
			characterFilter: ''
        }

        this.handleCharacterChange = this.handleCharacterChange.bind(this);
        this.searchButtonClicked = this.searchButtonClicked.bind(this);
    }


    handleCharacterChange(e) {
        const input = e.target.value
        this.setState({ characterFilter: input })
    }

    searchButtonClicked() {
        if (this.props.onClick) {
            this.props.onClick(this.state.characterFilter)
        }
    }

    render() {
        return (
            <>
                <div className="search">
                    <input type="text"
                            onChange={this.handleCharacterChange}
                            className="form-control"
                            placeholder="Search for a Rick &
                            Morty characater..." />
                        <button className="btn btn-light" onClick={this.searchButtonClicked}>Search</button>
                </div>
            </>
        )
    }
}

export default CharacterSearch