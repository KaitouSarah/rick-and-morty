import React from 'react'
import { Link } from 'react-router-dom'

let linkButton = null;

const locationCard = (props) => {
    linkButton = (
    <Link to={{ 
        pathname: '/location/' + props.id }} 
        className="btn btn-light">
        View
    </Link>);

    return (
        <div className="col-xs-12 col-sm-6 col-md-4">
            <div className="card CharacterCard mb-3" id="CharacterCard">
                <img src="http://2.bp.blogspot.com/-fJG0fnaJIKA/VdrO5GYG3RI/AAAAAAAAAzs/ErVZleaAPlk/s1600/RAM_205_BG_1FlashySolarSystem_Concept_v2_McD.jpg" alt="{props.name}" className="card-img-top"/>

                <div className="card-body">
                    <h4 className="card-title">{props.name}</h4>
                    <b>Type: </b> {props.type} <br />
                    <b>Dimension: </b> {props.dimension} <br />
                    {props.showLink ? linkButton : null}                   
                </div>
            </div>
        </div>
    );

}

export default locationCard;