import React, {Component} from 'react';
import LocationCard from "../../../components/LocationCard/LocationCard"

//LOCATION CARD PAGE
class CardPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: {}
        };
    }

    getLocationData(id) {
		fetch("https://rickandmortyapi.com/api/location/"+id)
		.then(response => response.json())
		.then(data => {
			if(!data.error){
                this.setState({location: data})
			}
		});
    };
    
    render() {
        let locationCard = null;

        if (this.state.location.id) {
			locationCard = (
				<LocationCard
					key={this.state.location.id}
					id={this.state.location.id}
					name={this.state.location.name}
					type={this.state.location.type}
					dimension={this.state.location.dimension}>
                </LocationCard>
            )    
        } else {
            locationCard = <kbd className="kbd warning">Loading...</kbd>;
        }
        return (
            <React.Fragment>{locationCard}</React.Fragment>
        )
    }

    componentDidMount() {
        this.getLocationData(this.props.match.params.id);
    }
}


export default CardPage;