import React, {Component} from 'react';
import LocationCard from "../../../components/LocationCard/LocationCard"
import LocationSearch from '../../../components/LocationSearch/LocationSearch';

//LOCATION CARDS PAGE
class CardsPage extends Component {
	constructor (props) {
		super(props);
		
		this.state = {
			rickMorty: [],
			locationCards: [], 
			statusCode: {}
		}; 
		this.getData("");
	};

	getData(input) {
		var copy;
		fetch("https://rickandmortyapi.com/api/location/?name="+input)
		.then(function (response) {
			copy = response.clone();
			return response.json();
		})
		.then(data => {
			if(data.results){
				this.setState({locationCards: data.results})
			} else if (copy.status === 429){
				this.setState({statusCode: 429});
			}
		});
		/* 	
		Arrow funtion that did not work when I wanted to 
		add a check for status code 429 (Too many requests for api)

		fetch("https://rickandmortyapi.com/api/location/")
		.then(response => response.json())
		.then(data => {
			if(data.results){
				this.setState({locationCards: data.results})
			} else {
				
			}
		});
		*/
	};

    render() {
		let locationCard = null;

        if (this.state.locationCards.length > 0) {
			locationCard = this.state.locationCards.map(location => (
				(<LocationCard
					key={location.id}
					id={location.id}
					name={location.name}
					type={location.type}
					dimension={location.dimension}
					showLink={true}>
				</LocationCard>)
			))       
		} else if(this.state.statusCode === 429) {
			locationCard = <kbd className="kbd error">DANG! Your IP-address has made too many requests to the Rick and Morty API :c</kbd>
		} else {
            locationCard = <kbd className="kbd warning">Loading...</kbd>;
        }
  
        return (
			<>
				<h4>LOCATIONS</h4>
				<br />
				<div className="searchComponent">
					<LocationSearch onClick={(filterSearch)=>{this.getData(filterSearch)}}/>
				</div>
				<div className="row">
					{locationCard}
				</div>
			</>
		);
    }
	//(prev, inut) => next
}
export default CardsPage;