import React, {Component} from 'react';
import CharacterCard from "../../../components/CharacterCard/CharacterCard"
import CharacterSearch from '../../../components/CharacterSearch/CharacterSearch';

//CHARACTER CARDS PAGE
class CardsPage extends Component {
	constructor (props) {
		super(props);
		
		this.state = {
			rickMorty: [],
			characterCards: [], 
			statusCode: {}
		}; 
		this.getData("");
	};

	getData(input) {
		var copy;
		fetch("https://rickandmortyapi.com/api/character/?name="+input)
		.then(function (response) {
			copy = response.clone();
			return response.json();
		})
		.then(data => {
			if(data.results){
				this.setState({characterCards: data.results})
			} else if (copy.status === 429){
				this.setState({statusCode: 429});
			}
		});
		/* 	
		Arrow funtion that did not work when I wanted to 
		add a check for status code 429 (Too many requests for api)

		fetch("https://rickandmortyapi.com/api/character/")
		.then(response => response.json())
		.then(data => {
			if(data.results){
				this.setState({characterCards: data.results})
			} else {
				
			}
		});
		*/
	};

    render() {
		let characterCard = null;

        if (this.state.characterCards.length > 0) {
			characterCard = this.state.characterCards.map(character => (
				(<CharacterCard
					key={character.id}
					id={character.id}
					image={character.image}
					name={character.name}
					species={character.species}
					status={character.status}
					gender={character.gender}
					location={character.location.name}
					origin={character.origin.name}
					showLink={true}>
				</CharacterCard>)
			))       
		} else if(this.state.statusCode === 429) {
			characterCard = <kbd className="kbd error">DANG! Your IP-address has made too many requests to the Rick and Morty API :c</kbd>
		} else {
            characterCard = <kbd className="kbd warning">Loading...</kbd>;
        }
  
        return (
			<>
				<h4>CHARACTERS</h4>
				<br />
				<div className="searchComponent">
					<CharacterSearch onClick={(filterSearch)=>{this.getData(filterSearch)}}/>
				</div>
				<div className="row">
					{characterCard}
				</div>
			</>
		);
    }
	//(prev, inut) => next
}
export default CardsPage;