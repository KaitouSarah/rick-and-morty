import React, {Component} from 'react';
import CharacterCard from "../../../components/CharacterCard/CharacterCard"

//CHARACTER CARD PAGE
class CardPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            character: {}
        };
    }

    getCharacterData(id) {
		fetch("https://rickandmortyapi.com/api/character/"+id)
		.then(response => response.json())
		.then(data => {
			if(!data.error){
                this.setState({character: data})
			}
		});
    };
    
    render() {
        let characterCard = null;

        if (this.state.character.id) {
			characterCard = (
				<CharacterCard
					key={this.state.character.id}
					id={this.state.character.id}
					image={this.state.character.image}
					name={this.state.character.name}
					species={this.state.character.species}
					status={this.state.character.status}
					gender={this.state.character.gender}
					location={this.state.character.location.name}
					origin={this.state.character.origin.name}>
                </CharacterCard>
            )    
        } else {
            characterCard = <kbd className="kbd warning">Loading...</kbd>;
        }
        return (
            <React.Fragment>{characterCard}</React.Fragment>
        )
    }

    componentDidMount() {
        this.getCharacterData(this.props.match.params.id);
    }
}


export default CardPage;