import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css'; 
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './App.css'
import { NavLink } from 'react-router-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import CharacterCardsPage from './containers/CharacterCards/CardsPage/CardsPage';
import CharacterCardPage from './containers/CharacterCards/CardPage/CardPage';
import LocationCardsPage from './containers/LocationCards/CardsPage/CardsPage';
import LocationCardPage from './containers/LocationCards/CardPage/CardPage';

ReactDOM.render(
  <BrowserRouter>
    <App>
      <header>
        <div className="navBar">
				  <NavLink className="nav-link" to="/">
					  Characters
				  </NavLink>
				  <NavLink className="nav-link" to="/locations">
					  Locations
				  </NavLink>
				</div>

        <h1>Rick and Morty</h1>
        <h2>New season comming this winter!</h2>
      </header>
      <div className="AppBody">
        <Route path="/" exact component={CharacterCardsPage} />
        <Route path="/character/:id" component={CharacterCardPage} />
        <Route path="/locations" exact component={LocationCardsPage} />
        <Route path="/location/:id" component={LocationCardPage} />
      </div>
    </App>
  </BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
