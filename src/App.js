import React from 'react';
import './App.css';
import './containers/CharacterCards/CardsPage/CardsPage'
import './containers/LocationCards/CardsPage/CardsPage'

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        {this.props.children}
      </div>
    );
  }
}
//<App/> creates an instance of app
//(App) passin the whole class
export default App;
